/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.37-log : Database - login
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`login` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `login`;

/*Table structure for table `user` */

CREATE TABLE `user` (
  `pk_id` bigint(20) NOT NULL COMMENT '主键',
  `nickname` varchar(50) DEFAULT NULL COMMENT '用户昵称',
  `headimgurl` varchar(200) DEFAULT NULL COMMENT '用户头像',
  `unionid` varchar(50) DEFAULT NULL COMMENT '用户统一标识。针对一个微信开放平台帐号下的应用，同一用户的unionid是唯一的。',
  PRIMARY KEY (`pk_id`),
  UNIQUE KEY `user_unionid_uindex` (`unionid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
