# 微信登录 APP版 后端篇 uniapp springboot+mybatis-plus

#### 介绍
微信登录 APP版 后端篇 springboot+mybatis-plus (源码+视频讲解) 通俗易懂 简单上手

#### 源码分享

| 前后端 | 项目地址  |
|---|---|
| 前端 | [代码地址](https://gitee.com/little-turtle-z/wechat-login)  |
|  后端 | [代码地址](https://gitee.com/little-turtle-z/wechat-login-back)  |



#### 框架版本
- springboot 2.6.14-SNAPSHOT
- mysql 5.7.37
- mybatis-plus 3.5.1
- mysql-connector-java 8.0.30
- hutool-all 5.8.2


#### 项目展示效果
 **- 未登录效果 (退出状态)** 

 ![输入图片说明](https://foruda.gitee.com/images/1667782595306430811/5fb530b6_8992400.jpeg "1.jpg") 
 ![输入图片说明](https://foruda.gitee.com/images/1667782635246736550/e52cdcae_8992400.jpeg "2.jpg")

 **-已登录效果** 
 
![输入图片说明](https://foruda.gitee.com/images/1667782664242930734/996b1929_8992400.jpeg "3.jpg")
![输入图片说明](https://foruda.gitee.com/images/1667782676628914678/a16ae8cd_8992400.jpeg "4.jpg")


#### 项目目录介绍

 ![输入图片说明](https://foruda.gitee.com/images/1667782866744987294/4d64507e_8992400.png "mysql.png")

#### 视频教程

> 关于功能的动态详讲解(讲解文档流程->代码的过程讲解->注意事项)
>
>我专门录制的一期B站视频
>
>【微信授权登录 | 全过程讲解[官方文档->代码梳理->注意点] APP版(附Gitee源码)】 
>
>[B站视频链接](https://www.bilibili.com/video/BV1Rv4y1D7Cr)
>
>🥰制作不易  感谢大家三连支持🥰


