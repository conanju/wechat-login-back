package com.little.wxlogin.constant;

/**
 * @Description 系统常量
 * @Author 小乌龟
 * @Date 2022/10/30 20:10
 */
public class SystemConstant {
    //APP
    /**
     * 应用唯一标识，在微信开放平台提交应用审核通过后获得
     */
    public static final String APPID="APP版微信appid";
    /**
     * 应用密钥AppSecret，在微信开放平台提交应用审核通过后获得
     */
    public static final String SECRET ="APP版微信secret";

    //小程序
    /**
     * 应用唯一标识，在微信开放平台提交应用审核通过后获得
     */
    public static final String APPLETS_APPID="小程序APPID";
    /**
     * 应用密钥AppSecret，在微信开放平台提交应用审核通过后获得
     */
    public static final String APPLETS_SECRET ="小程序微信secret";


    public static final int NUM_FOUR =4;

}
