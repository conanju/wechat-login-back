package com.little.wxlogin.mapper;

import com.little.wxlogin.pojo.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 小王八
 * @since 2022-10-30
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
