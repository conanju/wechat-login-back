package com.little.wxlogin.controller;


import com.little.wxlogin.pojo.vo.ReWxUserInfoVO;
import com.little.wxlogin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 小王八
 * @since 2022-10-30
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("/getUserLogin")
    public ReWxUserInfoVO getUserLogin(@RequestParam String code){
        return userService.loginByWx(code);
    }

    @GetMapping("/getUserLoginByApplets")
    public ReWxUserInfoVO getUserLoginByApplets(@RequestParam String code){
        return userService.loginByWxApplets(code);
    }

}
