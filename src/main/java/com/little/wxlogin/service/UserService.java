package com.little.wxlogin.service;

import com.little.wxlogin.pojo.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.little.wxlogin.pojo.ro.WxUserInfoRO;
import com.little.wxlogin.pojo.vo.ReWxUserInfoVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 小王八
 * @since 2022-10-30
 */
public interface UserService extends IService<User> {

     /**
       *微信登录验证 APP版
       * @param code 微信code
       * @return ReWxUserInfoVO
       * @author zhangjunrong
       * @date 2022/10/31 18:46
       */
     ReWxUserInfoVO loginByWx(String code);

     /**
      * 微信登录 小程序版
      * @param code
      * @return
      */
     ReWxUserInfoVO loginByWxApplets(String code);
}
