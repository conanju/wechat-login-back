package com.little.wxlogin.pojo.vo;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @Description 返回前端微信个人信息
 * @Author 小乌龟
 * @Date 2022/10/31 18:44
 */
@Data
@Accessors(chain = true)
public class ReWxUserInfoVO {
    /**
     * 用户id
     */
    private Long pkId;
    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 用户头像
     */
    private String headimgurl;

}
