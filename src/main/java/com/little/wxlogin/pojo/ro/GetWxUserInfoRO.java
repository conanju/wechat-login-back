package com.little.wxlogin.pojo.ro;

import lombok.Data;

/**
 * @Description 获取微信用户信息 参数
 * @Author 小乌龟
 * @Date 2022/10/30 20:28
 */
@Data
public class GetWxUserInfoRO {
    /**
     *调用接口凭证
     */
    private String access_token;
    /**
     *普通用户标识，对该公众帐号唯一
     */
    private String openid;
}
