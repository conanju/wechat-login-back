package com.little.wxlogin.pojo.ro;

import lombok.Data;

/**
 * @Description TODO
 * @Author 小乌龟
 * @Date 2022/12/19 14:40
 */
@Data
public class AppletsWxLoginRO {
    /**
     *普通用户标识，对该公众帐号唯一
     */
    private String openid;

    /**
     *用户在开放平台的唯一标识符
     */
    private String unionid;
}
