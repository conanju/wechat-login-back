package com.little.wxlogin.pojo.ro;

import lombok.Data;

/**
 * @Description 微信用户基础信息
 * @Author 小乌龟
 * @Date 2022/10/30 20:44
 */
@Data
public class WxUserInfoRO {
    /**
     * 用户昵称
     */
    private String nickname;

    /**
     * 用户头像
     */
    private String headimgurl;
    /**
     * 用户微信统一标识
     */
    private String unionid;

}
